Studentin:Lana Ajan Alhdid (357191)
Partner: Sören Ritzloff

Aufgabe1)

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt



def mult_table(n):
    # baut das array von 1 bis 10 auf
    x = np.arange(1, n+1)
    # baut in die vertikale und durch das multipizieren kommt das ganze 1x1 zustande.
    return x * x [:, None]
print(mult_table(10))


Aufgabe2)

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


df = pd.read_csv('wetterdaten.csv')
# Kommas werden durch punkte ersetzt, wegen engl. Schreibweise
df['Lufttemperatur (Jahresmittel)'] = df['Lufttemperatur (Jahresmittel)'].str.replace(',', '.')
# erst Zugriff auf spalte, dann umwandeln in numerisch, dann error für nicht numerische Daten.
df['Lufttemperatur (Jahresmittel)'] = pd.to_numeric(df['Lufttemperatur (Jahresmittel)'],errors='coerce' )
# Jahresdurchschnitt
print(df['Lufttemperatur (Jahresmittel)'].mean())

mean = df['Lufttemperatur (Jahresmittel)'].mean()
print(mean)
# methode für das errechnen der Abweichung
# erstellung eins leeren Array
deviations = np.array([])
# for schleife für das mehrmalige Widerholen
for x in df['Lufttemperatur (Jahresmittel)']:
    # nun ist die Bedinging, dass der Wert aus x - Durchschnitt genommen wird
   deviations = np.append(deviations, x - mean)

print('Abweichungen:')
print(deviations)
# Parameter für die Visualisierung
s = pd.Series(deviations, index=range(2000, 2020, 1))
# visualisierung
s.plot()
plt.show()
#speichern
s.plot()
plt.savefig('diviations.png')


Aufgabe 3)

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# einlesen
df  = pd.read_csv('RKI_COVID19_Niedersachsen.csv')
# Zugriff auf Splanten
df = df[['Landkreis', 'Geschlecht', 'AnzahlTodesfall', 'AnzahlGenesen', 'AnzahlFall']]
# für Orientierung
print('Hildesheim')
# Summe der TF im LK Hildesheim
print('Gesamt Todesfälle: ', df['AnzahlTodesfall'].loc[df['Landkreis'] == 'LK Hildesheim'].sum())
# Diversifizierung zwischen M, W, D
print('Männlich: ', df['AnzahlTodesfall'].loc[(df['Landkreis'] == 'LK Hildesheim') & (df['Geschlecht'] == 'M')].sum())
print('Weiblich: ', df['AnzahlTodesfall'].loc[(df['Landkreis'] == 'LK Hildesheim') & (df['Geschlecht'] == 'W')].sum())
print('Divers: ', df['AnzahlTodesfall'].loc[(df['Landkreis'] == 'LK Hildesheim') & (df['Geschlecht'] == 'D')].sum())
# Summen der Falltypen
sum_tot = df['AnzahlTodesfall'].loc[df['Landkreis'] == 'LK Hildesheim'].sum()
sum_genesen = df['AnzahlGenesen'].loc[df['Landkreis'] == 'LK Hildesheim'].sum()
sum_AlleFälle = df['AnzahlFall'].loc[df['Landkreis'] == 'LK Hildesheim'].sum()


# Methode zum errechnen ob die Zahlen valide sind, dafür werden die toten und genesenden zusammengerechnet. Die zahlen
# sind zwar valide, die Fälle, welche aktuell Covid haben werden aber nicht berücksichtigt.

if sum_AlleFälle == sum_tot + sum_genesen:
    print('Valid!')
else:
    print('Invalid!')


# Wiederholt wird alles mit den Zahlen für komplett Niedersachsen

df = df[['Geschlecht', 'AnzahlTodesfall', 'AnzahlGenesen', 'AnzahlFall']]
print('Niedersachsen')
print('Gesamt Todesfälle: ', df['AnzahlTodesfall'].sum())

print('Männlich: ', df['AnzahlTodesfall'].loc[(df['Geschlecht'] == 'M')].sum())
print('Weiblich: ', df['AnzahlTodesfall'].loc[(df['Geschlecht'] == 'W')].sum())
print('Divers: ', df['AnzahlTodesfall'].loc[(df['Geschlecht'] == 'D')].sum())

sum_tot = df['AnzahlTodesfall'].sum()
sum_genesen = df['AnzahlGenesen'].sum()
sum_AlleFälle = df['AnzahlFall'].sum()

# Methode zum errechnen ob die Zahlen valide sind, dafür werden die toten und genesenden zusammengerechnet. Die zahlen
# sind zwar valide, die Fälle, welche aktuell Covid haben werden aber nicht berücksichtigt.
if sum_AlleFälle == sum_tot + sum_genesen:
    print('Valid!')
else:
    print('Invalid!')

# Visualisierung
df = pd.read_csv('RKI_COVID19_Niedersachsen.csv')
df = df[['Meldedatum', 'Landkreis', 'AnzahlTodesfall']]
df = df.sort_values(by = ['Meldedatum'])
# Ausgabe
plt.plot(df['Meldedatum'], df['AnzahlTodesfall'])
plt.show()
# Speichern der Visualisierung
plt.plot(df['Meldedatum'], df['AnzahlTodesfall'])
plt.savefig('LK_Hildesheim_Visual.png')




